import { AfterViewInit, Component, DoCheck, OnChanges, SimpleChanges, ViewChild } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
        <ph-chat-entry-map [performInitialGeolocationLookup]="true"></ph-chat-entry-map>
  `,
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  @ViewChild('welcome', {static: false}) welcomeEl;
  @ViewChild('byebye', {static: false}) byeEl;

  title = 'awesome-ng';
  welcome = true;

  // ngAfterViewInit(): void {
  //   console.log('View Init', this.welcomeEl, this.byeEl);
  // }

  toggle() {
    this.welcome = !this.welcome;
  }

  // ngDoCheck(): void {
  //   console.log('Do Check', this.welcomeEl, this.byeEl);
  // }
}
