import {Injectable} from '@angular/core';
import { ChatItemOption } from './session.service';
// import {ChatItemDef, ChatItemOption, ChatItemType, SessionService, Status} from '../../core/session.service';
// import {BreakdownService, Car} from '../../core/breakdown.service';
// import {TranslateService} from '@ngx-translate/core';

@Injectable({
    providedIn: 'root'
})
export class ChatService {

    // private _chatEntries: ChatEntry[] = [];
    // private currChatItemDefIdx: number;
    // private currChatItemDef: ChatItemDef;
    // private chatItemDefs: ChatItemDef[];
    // private _chatData: Object = {};
    //
    // constructor(
    //     private sessionSvc: SessionService,
    //     private breakdownSvc: BreakdownService,
    //     private translateSvc: TranslateService) {
    //     if(this.sessionSvc.sessionId === 'AmountPassengers') {  // todo remove test code
    //         this._chatData['rest.residence-city'] = 'Lausanne';
    //     }
    // }
    //
    // startChat() {
    //     this.currChatItemDefIdx = 0;
    //     console.log('startChat ' + this.sessionSvc.status);
    //     this.chatItemDefs = this.sessionSvc.flavorDef.chat[this.sessionSvc.status];
    //     this.poseNextQuestion();
    // }
    //
    // get chatEntries(): ChatEntry[] {
    //     return this._chatEntries;
    // }
    //
    // get currentChatItemDef() {
    //     return this.currChatItemDef;
    // }
    //
    // poseNextQuestion() {
    //     let nextQuestionFound = false;
    //     console.log('poseNextQuestion ' + this.currChatItemDefIdx);
    //
    //     for (let i = this.currChatItemDefIdx; i < this.chatItemDefs.length; i++) {
    //         const chatItemDef = this.chatItemDefs[i];
    //         console.log('chatItemDef id: ' + chatItemDef.id + ' ' + chatItemDef.condition);
    //         if (!chatItemDef.condition || this.isTrue(chatItemDef.condition)) {
    //             nextQuestionFound = true;
    //             this.currChatItemDefIdx = i;
    //             this.currChatItemDef = chatItemDef;
    //             this.currChatItemDef.value = this._chatData[chatItemDef.id];     // already given answers
    //
    //             console.log('chatItemDef.hideQuestion:'+chatItemDef.hideQuestion);
    //             if(!chatItemDef.hideQuestion) {
    //                 const type = chatItemDef.type === ChatItemType.TXT_MULTI ?
    //                     ChatEntryType.BOT_MULTI : ChatEntryType.BOT_SINGLE;
    //                 this._chatEntries.push({
    //                     id: this.currChatItemDef.id,
    //                     type:  type,
    //                     text: this.getChatText(chatItemDef)
    //                 });
    //             }
    //
    //             switch (chatItemDef.type) {
    //                 case ChatItemType.Q_GEO:
    //                     setTimeout(() => this.geoLocate(), 1000);
    //                     break;
    //                 case ChatItemType.TXT_SINGLE:
    //                 case ChatItemType.TXT_MULTI:
    //                     this.currChatItemDefIdx++;
    //                     setTimeout(() => this.poseNextQuestion(), 1000);
    //                     break;
    //             }
    //             break;
    //         }
    //     }
    //     if (!nextQuestionFound) {
    //         this.currChatItemDef = {
    //             id: 'end.dummy',
    //             type: ChatItemType.TXT_SINGLE
    //         };
    //         this.sendData();
    //     }
    // }
    //
    processAnswer(displayValue: string, dataValue: any, hideAnswer?:boolean) {
    //     //console.log('ChatService.processAnswer for ' + this.currChatItemDefIdx + ' ' + answer);
    //     if(!hideAnswer) {
    //         let translatedText = this.translateSvc.instant(displayValue);
    //         this._chatEntries.push({
    //             id: this.currChatItemDef.id,
    //             type: ChatEntryType.USER,
    //             text: translatedText
    //         });
    //     }
    //     this._chatData[this.chatItemDefs[this.currChatItemDefIdx].id] = dataValue;
    //     this.currChatItemDefIdx++;
    //     this.poseNextQuestion();
    }
    //
    getOptionText(option: ChatItemOption): string {
    //     let translatedText = this.translateSvc.instant(option.text);
    //     return this.getSubstitutedText(translatedText, option.args);
      return 'TEST';
    }
    //
    getAnswer(questionId:string): any {
    //     return this._chatData[questionId];
    }
    //
    // private isTrue(condition: string) {
    //     let f = new Function('chatData', 'return ' + condition);
    //     return f(this._chatData);
    // }
    //
    // private sendData() {
    //     switch (this.sessionSvc.status) {
    //         case Status.IdentifyContact:
    //         case Status.NoCoverage:
    //             //
    //             if(this._chatData['chat.input-summary'] === 'option.yes') {
    //                 this.sendIdentifyContactData();
    //             } else {
    //                 this.startChat();
    //             }
    //             break;
    //         case Status.BrandModel:
    //             this.sendBrandModel();
    //             break;
    //     }
    // }
    //
    // private sendBrandModel() {
    //     if( this._chatData['chat.breakdown-reason'] === 'option.accident' ||
    //         this._chatData['chat.confirm-model'] === 'option.no') {
    //         //todo REST call ErrorCase
    //         console.log('send ErrorCase');
    //         return;
    //     }
    //
    //     const car = this.getCar();
    //     let breakdownReason = '';
    //     let problemDescription = '';
    //     switch(this._chatData['chat.breakdown-reason']) {
    //         case 'option.doesnt-start':
    //             breakdownReason = '3007';
    //             break;
    //         case 'option.engine-out':
    //             breakdownReason = '3000';
    //             break;
    //         case 'option.flat-tire':
    //             breakdownReason = this._chatData['chat.spare-tire'] === 'option.yes' ? '3003': '3004';
    //             break;
    //         case 'option.fuel-problem':
    //             if(this._chatData['fuel-problem-start'] === 'option.yes')  breakdownReason = '3008';
    //             if(this._chatData['fuel-problem-start'] === 'option.no')  breakdownReason = '3009';
    //             if(this._chatData['chat.fuel-problem-details'] === 'option.no-fuel') problemDescription = 'Tank leer';
    //             break;
    //         case 'option.misc':
    //             problemDescription = this._chatData['chat.breakdown-misc'];
    //             break;
    //     }
    //     const gearbox = this._chatData['chat.gearbox'] === 'option.manual' ? '180001' : '180004';
    //
    //     console.log('sendBrandModel make=' + car.make + ',model=' + car.model + ',breakdownReason=' +
    //         breakdownReason + ',problemDescription=' + problemDescription + ',gearbox=' + gearbox);
    //     this.breakdownSvc.brandModel(this.sessionSvc.sessionId, car.make, car.model, breakdownReason,
    //         problemDescription, gearbox, '', '', '', '');
    // }
    //
    // private sendIdentifyContactData() {
    //     this.breakdownSvc.identifyContact(this.sessionSvc.sessionId, this._chatData['chat.first-name'],
    //         this._chatData['chat.last-name'], this._chatData['chat.birth-date'], this._chatData['chat.license-num']
    //     ).subscribe(
    //         resp => {
    //             console.log('sendIdentifyContactData success response ' + resp.status);
    //             this.sessionSvc.setScreen(resp.status);
    //             switch (resp.status) {
    //                 case Status.BrandModel:
    //                     if(resp.residenceCity) {
    //                         this._chatData['rest.residence-city'] = resp.residenceCity;
    //                     }
    //                     if (resp.cars && Array.isArray(resp.cars.car)) {    // car data available
    //                         this._chatData['rest.car-count'] = resp.cars.car.length;
    //                         if (resp.cars.car.length === 1) {
    //                             this._chatData['rest.make'] = resp.cars.car[0].make;
    //                             this._chatData['rest.model'] = resp.cars.car[0].model;
    //                         } else if (resp.cars.car.length > 1) {
    //                             this.setCarOptions(resp.cars.car);
    //                         }
    //                     }
    //                     this.startChat();
    //                     break;
    //
    //                 case Status.NoCoverage:
    //                     this.startChat();
    //                     break;
    //             }
    //         },
    //         errorResp => {
    //             console.log('sendIdentifyContactData error response ' + JSON.stringify(errorResp));
    //             this.sessionSvc.setScreen(Status.ErrorCase);
    //         }
    //     );
    // }
    //
    // private getChatText(chatItemDef: ChatItemDef) {
    //     let translatedText = this.translateSvc.instant(chatItemDef.id);
    //     return this.getSubstitutedText(translatedText, chatItemDef.args);
    // }
    //
    // private getSubstitutedText(text:string, args: string[]) {
    //     if (args) {
    //         for (let i = 0; i < args.length; i++) {
    //             text = text.replace('%' + (i + 1), this._chatData[args[i]]);
    //         }
    //     }
    //     return text;
    // }
    //
    // private setCarOptions(cars: Car[]) {
    //     const chatItemDefs = this.sessionSvc.flavorDef.chat.BrandModel;
    //     for (let i = 0; i < chatItemDefs.length; i++) {
    //         if (chatItemDefs[i].id === 'chat.which-model') {
    //             cars.forEach(car => chatItemDefs[i].options.push({
    //                 text: car.make + ' ' + car.model,
    //                 value: car,
    //                 flex: 1,
    //                 icon: 'car',
    //                 align: 'left'
    //             }));
    //             break;
    //         }
    //     }
    // }
    //
    // private getCar():Car {
    //     switch(this._chatData['rest.car-count'] || 0) {
    //         case 0:
    //             const brandModel = this._chatData['chat.model'].split(' ');
    //             return { make: brandModel[0], model:  brandModel.length > 1 ? brandModel[1] : '' };
    //         case 1:
    //             return { make: this._chatData['rest.make'], model: this._chatData['rest.model'] };
    //         default:
    //             return this._chatData['chat.which-model'];
    //     }
    // }
    //
    // private geoLocate() {
    //     if (navigator.geolocation) {
    //         navigator.geolocation.getCurrentPosition( (position: Position) => {
    //             console.log( 'Latitude: ' + position.coords.latitude +
    //                 ' Longitude: ' + position.coords.longitude);
    //             //TODO transform into addressLine
    //             this.processAnswer('option.location-identified', position.coords, true);
    //         }, (error: PositionError) => {
    //             console.log('geolocation error: ' + error.message);
    //             this.processAnswer('option.no-geolocation', 'option.no-geolocation', true);
    //         });
    //     } else {
    //         console.log('Geolocation is not supported');
    //         this.processAnswer('option.no-geolocation', 'option.no-geolocation', true);
    //     }
    // }
}

export enum ChatEntryType {
    USER = 'user',
    BOT_SINGLE = 'bot',
    BOT_MULTI = 'bot2'
}

export interface ChatEntry {
    id: string;
    type: ChatEntryType;
    text: string;
}
