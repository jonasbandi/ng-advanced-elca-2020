import GeocoderAddressComponent = google.maps.GeocoderAddressComponent;

export type PositionType = 'SELECTION' | 'GEOLOCATION';

export interface Address {
  address?: string;
  zipCode: string;
  city: string;
  country: string;
  display: string;
  positionType: PositionType;
}

export function isInServiceArea(address: Address) {
  return ['CH', 'LI'].includes(address.country);
}

export function createAddress(addresses: {address_components?: GeocoderAddressComponent[]}[], positionType: PositionType) {
  console.log('create address from geocode result', addresses);
  if (!addresses || !addresses[0]) {
    return null;
  } else {
    const addr = addresses[0];
    //this.addressLine = results[0].formatted_address;
    //console.log('createAddress ' + JSON.stringify(addresses[0]));
    let address = getAddrComponent(addr, 'route');
    const houseNr = getAddrComponent(addr, 'street_number');
    if (houseNr) address += ' ' + houseNr;
    if(address === 'Unnamed Road') address = undefined;
    let city = getAddrComponent(addr, 'locality');
    if(city === '') city = getAddrComponent(addr, 'administrative_area_level_2');
    const country = getAddrComponent(addr, 'country');
    const zipCode = getAddrComponent(addr, 'postal_code');
    let display = zipCode + ' ' + city;
    if (address) display = address + ', ' + display;

    const addressLiteral: Address = {
      city: city,
      country: country,
      zipCode: zipCode,
      address: address,
      display: display,
      positionType
    };
    if(!isInServiceArea(addressLiteral)) {
      addressLiteral.display += ', ' +  getAddrComponent(addr, 'country', 'long_name');
    }

    return addressLiteral;
  }
}

export function getAddrComponent(result: any, name: string, property:string = 'short_name'): string {
  if (result && result.address_components) {
    for (let i = 0; i < result.address_components.length; i++) {
      if (result.address_components[i].types.includes(name)) {
        return result.address_components[i][property];
      }
    }
  }
  return '';
}

