import {ChangeDetectorRef, Component, ElementRef, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {ChatService} from '../chat.service';
import {LatLngLiteral, MapsAPILoader} from '@agm/core';
import {bindCallback, combineLatest, concat, from, merge, Observable, of, Subject} from 'rxjs';
import {distinctUntilChanged, filter, map, share, startWith, switchMap, tap} from 'rxjs/operators';
import {mapStyles} from './map.styles';
import {geolocatioError$, geolocation$} from './geolocation-observable';
import {Address, createAddress, getAddrComponent} from './address';

type MarkerType = 'SELECTION' | 'GEOLOCATION';
type Marker = {
  latLng: LatLngLiteral,
  type: MarkerType
};

export type GeocoderResponse = [
   google.maps.GeocoderResult[], google.maps.GeocoderStatus
];

let lookupAddressFn: (latLng: { latLng: { lat: number, lng: number } }) => Observable<GeocoderResponse>; // singleton


const DEFAULT_LOCATION: LatLngLiteral = {
  lat: 46.798439,
  lng: 8.231878
};

@Component({
  selector: 'ph-chat-entry-map',
  templateUrl: './chat-entry-map.component.html',
  styleUrls: ['./chat-entry-map.component.scss']
})
export class ChatEntryMapComponent implements OnInit {
  @ViewChild('autocomplete', {static: true}) autocompleteElementRef: ElementRef;
  @ViewChild('footer', {static: false}) footerRef: ElementRef;
  @Input() performInitialGeolocationLookup: boolean;
  @Output() closeMap: EventEmitter<boolean> = new EventEmitter<boolean>();

  private selectedAddress: Address;

  mapHeight = 250;
  mapStyles = mapStyles;
  center = DEFAULT_LOCATION;
  zoom = 8;

  // Source streams
  private mapLoaded$: Observable<void>;
  private mapClick$ = new Subject<LatLngLiteral>();
  private locateClick$ = new Subject<void>();
  private googlePlace$: Observable<google.maps.places.PlaceResult>;

  private geolocationTrigger$: Observable<void>;

  //Position streams
  private geolocationPosition$: Observable<Marker>;
  private mapClickPosition$: Observable<Marker>;
  private googlePlacePosition$: Observable<Marker>;

  // Address streams
  private geolocationAddress$: Observable<Address>;
  private mapClickAddress$: Observable<Address>;
  private googlePlaceAddress$: Observable<Address>;

  // Display streams
  private displayPosition$: Observable<Marker>;
  private displayAddress$: Observable<Address>;
  private showFooter$: Observable<boolean>;
  private geolocationFailure$: Observable<{ message: string }>;
  viewModel$: Observable<any>;

  constructor(
    private mapLoader: MapsAPILoader,
    private ref: ChangeDetectorRef,
    private chatSvc: ChatService,
    private _chatEntryMap: ElementRef) {
  }

  ngOnInit() {
    this.mapLoaded$ = from(this.mapLoader.load()).pipe(share());

    // Async initialization
    this.mapLoaded$
      .pipe(
        tap(() => {
          console.log('MAP LOADED - INITIALIZING GEOCODER');
          const geocoder = new google.maps.Geocoder();

          // lookupAddressFn = bindCallback<{ results: google.maps.GeocoderResult[], status: google.maps.GeocoderStatus }>(geocoder.geocode as any);
          lookupAddressFn = bindCallback<[google.maps.GeocoderResult[], google.maps.GeocoderStatus]>(geocoder.geocode as any);
        }),
      )
      .subscribe();

    this.googlePlace$ = this.mapLoaded$
      .pipe(
        switchMap(() => {
          const autocomplete = new google.maps.places.Autocomplete(this.autocompleteElementRef.nativeElement, {types: ['address']});
          autocomplete.setComponentRestrictions({'country': ['ch', 'li']});
          //this.autocompleteElementRef.nativeElement.focus();

          return new Observable<google.maps.places.PlaceResult>(observer => {
            autocomplete.addListener('place_changed', () => {
              const place: google.maps.places.PlaceResult = autocomplete.getPlace();
              if(getAddrComponent(place, 'postal_code') === '') {
                console.log('postal code lookup necessary ' + JSON.stringify(place));
              }
              observer.next(place);
            });
          });
        }),
        filter(place => !!place && !!place.geometry && !!place.geometry.location),
        tap((v) => console.log('SELECTED PLACE', v)),
        share()
      );

    this.geolocationTrigger$ = merge(
      this.mapLoaded$.pipe(filter(() => this.performInitialGeolocationLookup)),
      this.locateClick$.pipe(tap(() => console.log('Locate Click Stream emitted'))))
      .pipe(
        tap(() => console.log('GEOLOCATION TRIGGER')),
        tap(() => this.autocompleteElementRef.nativeElement.value = ''), // side-effect
        share(),
      );

    this.mapClickPosition$ = this.mapClick$
      .pipe(
        map((latLng): Marker => ({latLng, type: 'SELECTION'})),
        tap(() => this.autocompleteElementRef.nativeElement.value = ''), // side-effect
        share(),
      );

    this.googlePlacePosition$ = this.googlePlace$
      .pipe(
        map((place): LatLngLiteral => ({
          lat: place.geometry.location.lat(),
          lng: place.geometry.location.lng()
        })),
        tap((latLng) => {
          this.setCenter(latLng); // Side-Effect
        }),
        map((latLng): Marker => ({
            latLng: latLng,
            type: 'SELECTION'
          })
        ),
        tap((v) => console.log('SELECTED PLACE POSITION', v)),
        share()
      );

    this.geolocationPosition$ = this.geolocationTrigger$
      .pipe(
        tap(() => console.log('GEOLOCATION FIRED')),
        filter(() => !!navigator.geolocation),
        switchMap(() => geolocation$),
        map(position => ({
          lat: position.coords.latitude + (0.00000000001 * Math.random()),
          lng: position.coords.longitude + (0.00000000001 * Math.random())
        })),
        tap(latLng => {
          this.setCenter(latLng); // Side-Effect
        }),
        map((latLng): Marker => ({latLng, type: 'GEOLOCATION'})),
        tap((v) => console.log('GEOLOCATION', v)),
        filter(v => !!v),
        share()
      );

    this.mapClickAddress$ = this.mapClick$
      .pipe(
        switchMap<LatLngLiteral, Observable<GeocoderResponse>>((latLng: LatLngLiteral) => concat(
          of(null), // clear the current address
          lookupAddressFn({latLng}) // emit the result of the geocoder
        )),
        map(geocoderResponse => geocoderResponse && createAddress(geocoderResponse[0], 'SELECTION')),
        tap((v) => console.log('MAP CLICK ADDRESS', v)),
        share()
      );

    this.geolocationAddress$ = this.geolocationPosition$
      .pipe(
        switchMap<Marker, Observable<GeocoderResponse>>((marker: Marker) => concat(
          of(null), // clear the current address
          lookupAddressFn({latLng: marker.latLng}))
        ),
        tap((v) => console.log('GEOLOCATION ADDRESS', v)),
        map(geocoderResponse => geocoderResponse && createAddress(geocoderResponse[0], 'GEOLOCATION')),
        share()
      );


    this.googlePlaceAddress$ = this.googlePlace$
      .pipe(
        map((place): [Address, google.maps.places.PlaceResult] => {
            const address = createAddress([place], 'SELECTION');
            return [address, place];
          }),
        // map(([address, place]) => address), // DEMO: Replace with Geo Lookup
        switchMap(([address, place]) => {
          if (address.zipCode){
            console.log('GOOGLE PLACE ADDRESS HAS A ZIP CODE: ', address.zipCode);
            return of(address);
          } else {
            // The address from google places has no zip code (this might happen, when a street spans several zips codes)
            // However our application needs to pass a zip code to the backend, so we look up the zip code for the lat/lng of the given place
            console.log('GOOGLE PLACE ADDRESS HAS NO ZIP CODE - PERFORMING ADDRESS LOOKUP TO GET ZIP CODE ...');
            return lookupAddressFn({latLng: place.geometry.location.toJSON()})
              .pipe(
                map((geocoderResponse) => {
                  let lookedUpZipCode = "";
                  if (geocoderResponse && geocoderResponse.length > 0 && geocoderResponse[0].length > 0){
                    const responseData = geocoderResponse[0];
                    if (responseData.length > 0){
                      lookedUpZipCode = getAddrComponent(responseData[0], 'postal_code');
                    }
                  }
                  console.log('GOOGLE PLACE ADDRESS - LOOK UP FOUND ZIP CODE:', lookedUpZipCode);
                  address.zipCode = lookedUpZipCode;
                  address.display = address.display + ' ' + lookedUpZipCode;
                  return address;
                })
              )
          }
        }),
        tap((v) => console.log('GOOGLE PLACE ADDRESS', v)),
        share()
      );

    // Display streams
    this.displayPosition$ = merge(this.geolocationPosition$, this.mapClickPosition$, this.googlePlacePosition$)
      .pipe(
        startWith(undefined),
        tap((v) => console.log('DISPLAY POSITION', v)),
        share()
      );

    this.geolocationFailure$ = merge(
      geolocatioError$, // pass through the error
      this.displayPosition$.pipe(map(() => undefined)) // cancel the error as soon as a position is emitted
    )
      .pipe(
        startWith(undefined),
        distinctUntilChanged()
      );

    this.displayAddress$ = merge(this.geolocationAddress$, this.mapClickAddress$, this.googlePlaceAddress$)
      .pipe(
        startWith(undefined),
        tap((v) => console.log('DISPLAY ADDRESS', v)),
        tap(address => this.selectedAddress = address),
        share()
      );

    this.showFooter$ = merge(this.displayAddress$, this.geolocationFailure$)
      .pipe(
        map(() => true),
        startWith(false),
        distinctUntilChanged(),
        share()
      );

    this.viewModel$ = combineLatest([
      this.displayPosition$,
      this.geolocationPosition$.pipe(startWith(undefined)),
      this.displayAddress$,
      this.showFooter$,
      this.geolocationFailure$
    ]).pipe(
      map(([displayPosition, geolocationPosition, displayAddress, showFooter, geolocationFailure]) => {
        return {displayPosition, geolocationPosition, displayAddress, showFooter, geolocationFailure};
      }),
      tap((v) => console.log('VIEW MODEL', v)),
      tap(() => setTimeout(() => {
        this.ref.detectChanges();
        const footerHeight = this.footerRef ? this.footerRef.nativeElement.offsetHeight : 0;
        this.mapHeight = this._chatEntryMap.nativeElement.offsetHeight - footerHeight;
        console.log('set mapHeight:' + this.mapHeight);
      })), // google map api is using JSONP, which does not work with default Angular change detection
    );
  }

  mapClick(event) {
    const latLng: LatLngLiteral = {
      lat: event.coords.lat,
      lng: event.coords.lng
    };
    this.mapClick$.next(latLng);
  }

  locateClick() {
    console.log('Event: Locate Click');
    this.locateClick$.next();
  }

  submit() {
    console.log('ChatEntryMapComponent submit', this.selectedAddress);
    this.chatSvc.processAnswer(this.selectedAddress.display, this.selectedAddress);
    this.ref.detectChanges();
  }

  private setCenter(latLng: LatLngLiteral) {
    this.center = latLng;
    this.zoom = 14; //this.zoom + 0.5; // Hack: Setting the zoom does not work ...
  }

  closeMapClick() {
    this.closeMap.emit(true);
  }
}

