import {Observable, Subject} from 'rxjs';


export const geolocation$ = new Observable<Position>(observer => {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(
      position => {
        observer.next(position);
        observer.complete();
      },
      (error) => {
        observer.complete();
        geolocatioError$.next(error);
      }
    );
  } else {
    geolocatioError$.next({message: 'Geolocation is not supported!'});
  }
});

export const geolocatioError$ = new Subject<{ message: string }>();

// // Fake position for testing
// export const geolocation$ = new Observable<Position>(observer => {
//     console.log('Subscribing to geolocation');
//     if (navigator.geolocation) {
//         navigator.geolocation.getCurrentPosition(
//             position => {
//                 if (position && position.coords.longitude < 7.5) {
//                     observer.next(position);
//                     observer.complete();
//                 } else {
//                     // observer.error({message: 'Simulated geolocation error'});
//                     geolocatioError$.next({message: 'Simulated geolocation error'})
//                     observer.complete();
//                 }
//             },
//             (error) => geolocatioError$.next(error)
//         );
//     } else {
//         geolocatioError$.next({message: 'Geolocation is not supported!'});
//     }
// });
