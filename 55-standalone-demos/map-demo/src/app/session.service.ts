import {Injectable} from '@angular/core';
// import {TranslateService} from '@ngx-translate/core';
import {map, switchMap} from 'rxjs/operators';
// import {BreakdownService} from './breakdown.service';
import {HttpClient} from '@angular/common/http';

const DEFAULT_FLAVOR = 1;
const FLAVOR_COUNT = 2;
const LANGUAGES = {
    'DEU': 'de-CH',
    'FRA': 'fr-CH',
    'ITA': 'it-CH'
};

@Injectable({
    providedIn: 'root'
})
export class SessionService {
    private _language: string;
    private _sessionId: string;
    private _screen: AppScreen;
    private _flavorId: number;
    private _flavorDef: FlavorDef;
    private _status: Status;

    constructor(
        // private translateSvc: TranslateService,
        // private breakdownSvc: BreakdownService,
        private httpClient: HttpClient) {
        this.initParamsFromUrl();
    }

    get screen() {
        return this._screen;
    }

    setScreen(status:Status) {
        this._status = status;
        switch (status) {
            case Status.IdentifyContact:
                this._screen = this._flavorDef.showIntro && this._screen !== AppScreen.Intro ?
                    AppScreen.Intro : AppScreen.Chat;
                break;

            case Status.NoCoverage:
            case Status.BrandModel:
            case Status.AmountPassengers:
                this._screen = AppScreen.Chat;
                break;

            case Status.ErrorCase:
                this._screen = AppScreen.Error;
                break;

            default:
                this._screen = AppScreen.InvalidStatus;
                break;
        }
    }

    get sessionId() {
        return this._sessionId;
    }

    get status() {
        return this._status;
    }

    set status(status: Status) {
        this._status = status;
    }

    get flavorDef() {
        return this._flavorDef;
    }


    private loadFlavor() {
        // const flavorKey = 'siecc-pannenhilfe-rwc.' + this.sessionId + '.flavorId';
        // let storedFlavorId: string = localStorage.getItem(flavorKey);
        // if (storedFlavorId == null) {   // not in local storage yet
        //     this._flavorId = this._language === 'de-CH' ? Math.floor(Math.random() * FLAVOR_COUNT) + 1 : DEFAULT_FLAVOR;
        //     localStorage.setItem(flavorKey, this._flavorId.toString());
        //     console.log('SessionService.loadFlavor() set flavorId ' + this._flavorId);
        // } else {
        //     // let storedFlavorIdNum = Math.min(Math.max(parseInt(storedFlavorId), 1), FLAVOR_COUNT);
        //     this._flavorId = isNaN(storedFlavorIdNum) ? DEFAULT_FLAVOR : storedFlavorIdNum;
        //     console.log('SessionService.loadFlavor() reuse flavorId ' + this._flavorId);
        // }
        // //console.log('SessionService.loadFlavor() flavorId: ' + this._flavorId);
        // return this.httpClient.get<any>('assets/flavors/flavor-' + this._flavorId + '.json');
    }

    public initSession() {
        // return new Promise((resolve) => {
        //     if (this.sessionId) {
        //         this.breakdownSvc.getStatus(this.sessionId).pipe(
        //             switchMap(getStatusResp => this.loadFlavor().pipe(
        //                 map(flavorResp => ({getStatusResp, flavorResp}))
        //             ))
        //         ).subscribe(
        //             (combinedSuccessResponse) => {
        //                 this._flavorDef = combinedSuccessResponse.flavorResp;
        //                 this._status = combinedSuccessResponse.getStatusResp.status;
        //                 console.log('SessionService.initSession() status:' + this._status);
        //                 console.log('SessionService.initSession() show intro:' + this._flavorDef.showIntro);
        //
        //                 this.setScreen(this._status);
        //                 resolve();
        //             },
        //             (errorResponse) => {
        //                 console.log('SessionService.initSession() error: ', errorResponse);
        //                 this._screen = AppScreen.InvalidStatus;
        //                 resolve();
        //             }
        //         );
        //     } else {
        //         this._screen = AppScreen.InvalidStatus;
        //         resolve();
        //     }
        // });
    }

    private initParamsFromUrl() {
        // let urlParts = location.pathname.split('/');
        // if (urlParts.length >= 3) {
        //     this._sessionId = urlParts.pop();
        //     let lang = urlParts.pop().toUpperCase();
        //     if (!lang || !LANGUAGES[lang]) lang = 'DEU';
        //     this._language = LANGUAGES[lang];
        //     console.log('SessionService.initParamsFromUrl() ' + this._sessionId + ' ' + this._language);
        //
        // }
    }

    public loadTranslations() {
        // This method should be called when the application initialization and the application should only start after the Promise resolved
        // This should then allow the application to use 'translationService.instant' because the translations are guaranteed to be loaded.
        // this.translateSvc.setDefaultLang(this._language);
        // return this.translateSvc.use(this._language).toPromise();
    }
}

export enum AppScreen {
    Intro,
    Chat,
    InvalidStatus,
    Error
}

export enum Status {
    IdentifyContact = 'IdentifyContact',
    NoCoverage = 'NoCoverage',
    BrandModel = 'BrandModel',
    AmountPassengers = 'AmountPassengers',
    ErrorCase = 'ErrorCase'
}

export enum ChatItemType {
    TXT_SINGLE = 'txt',
    TXT_MULTI = 'txt2',
    Q_DATE = 'q-date',
    Q_STR = 'q-str',
    Q_LIST = 'q-list',
    Q_GEO = 'q-geo',
    Q_MAP = 'q-map'
}

export enum InputPattern {
    NUMBERPLATE = 'numberplate'
}

export interface ChatItemOption {
    text: string;
    value?: any;
    flex: number;
    align?: string;
    icon?: string;
    args?: string[];
    hideAnswer?: boolean;
}

export interface ChatItemDef {
    id: string;
    type: ChatItemType;
    condition?: string;
    args?: string[];
    value?: any;
    options?: ChatItemOption[];
    pattern?: InputPattern;
    hideQuestion?: boolean;
}

export interface FlavorDef {
    showIntro: boolean;
    chat: {
        IdentifyContact: ChatItemDef[],
        NoCoverage: ChatItemDef[],
        BrandModel: ChatItemDef[],
        AmountPassengers: ChatItemDef[]
    };
}
