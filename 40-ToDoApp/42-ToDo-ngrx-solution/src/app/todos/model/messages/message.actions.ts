import { Action, createAction, props } from '@ngrx/store';

export const showMessage = createAction('[Message] Show', props<{message: string}>());
