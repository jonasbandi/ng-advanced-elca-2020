export class ToDo {
  id: number;
  title: string;
  completed: boolean;

  constructor(title: string = '', id?: number) {
    this.id = id;
    this.title = title.trim();
    this.completed = false;
  }
}
