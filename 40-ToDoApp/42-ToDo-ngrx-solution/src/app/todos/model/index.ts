import {IMessageState, messageReducer} from './messages/message.store';
import {IToDosState, todoReducer} from './todos/todo.store';
import {ActionReducer, ActionReducerMap} from '@ngrx/store';
import {environment} from '../../../environments/environment';
import {storeLogger} from 'ngrx-store-logger';
import { storeFreeze } from 'ngrx-store-freeze';


interface AppState {
  todos: IToDosState;
  message: IMessageState;
}

export const reducers: ActionReducerMap<AppState> = {todos: todoReducer, message: messageReducer};

export const metaReducers = environment.production ? [] : [logger, storeFreeze];


function logger(reducer: ActionReducer<AppState>): any {
  // default, no options
  return storeLogger()(reducer);
}


