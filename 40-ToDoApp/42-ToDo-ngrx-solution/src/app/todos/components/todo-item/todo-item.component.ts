import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDo } from '../../model/todos/todo.model';

@Component({
  selector: 'td-todo-item',
  template: `
      <li>
          {{todo.title}}
          <button *ngIf="todo.id" class="remove-button" (click)="onRemoveToDo()">
              x
          </button>
      </li>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoItemComponent {

  @Input() todo: ToDo;
  @Output() removeToDo = new EventEmitter<ToDo>();

  onRemoveToDo() {
    this.removeToDo.emit(this.todo);
  }

}
