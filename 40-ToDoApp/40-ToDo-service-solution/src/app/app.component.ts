import { Component, OnInit } from '@angular/core';
import { ToDoStoreService } from './todos/model/todo-store.service';

@Component({
  selector: 'td-root',
  templateUrl: './app.component.html'
})
export class AppComponent implements OnInit {
  constructor(private toDoStoreService: ToDoStoreService) {}

  ngOnInit(): void {
    this.toDoStoreService.loadPendingTodos();
    this.toDoStoreService.loadCompletedTodos();
  }
}
