import { Injectable } from '@angular/core';
import { ToDo } from './todo.model';

import { ToDoApiService } from './todo-api.service';
import { of } from 'rxjs';
import { tap } from 'rxjs/operators';

const backendUrl = 'http://localhost:3456/todos';

@Injectable({ providedIn: 'root' })
export class ToDoStoreService {
  pendingTodos: ToDo[] = [];
  completedTodos: ToDo[] = [];

  constructor(private apiService: ToDoApiService) {}

  loadPendingTodos(): void {
    this.apiService.getTodos(false).subscribe(todos => (this.pendingTodos = todos));
  }

  loadCompletedTodos(): void {
    this.apiService.getTodos(true).subscribe(todos => (this.completedTodos = todos));
  }

  getTodo(id: number) {
    // Note: We assume that the ToDo is already loaded ... in typical projects this is not possible!
    const todo = [...this.pendingTodos, ...this.completedTodos].find(t => t.id === id);
    return of(todo);
  }

  addTodo(todo: ToDo): void {
    this.pendingTodos.push(todo);
    this.apiService.saveTodo(todo).subscribe(t => (todo.id = t.id));
  }

  updateTodo(todo: ToDo): void {
    let todoIndex: number;
    if (this.completedTodos) {
      todoIndex = this.completedTodos.findIndex(t => t.id === todo.id);
      if (todoIndex !== -1) {
        this.completedTodos[todoIndex] = todo;
      }
    }

    todoIndex = this.pendingTodos.findIndex(t => t.id === todo.id);
    if (todoIndex !== -1) {
      const original = this.pendingTodos[todoIndex];
      if (original.completed !== todo.completed) {
        this.pendingTodos.splice(todoIndex, 1);
        if (this.completedTodos) {
          this.completedTodos.push(todo);
        }
      } else {
        this.pendingTodos[todoIndex] = todo;
      }
    }

    this.apiService.updateTodo(todo).subscribe();
  }

  deleteTodo(todo: ToDo): void {
    this.completedTodos = this.completedTodos.filter(t => t !== todo);
    this.apiService.deleteTodo(todo).subscribe();
  }
}
