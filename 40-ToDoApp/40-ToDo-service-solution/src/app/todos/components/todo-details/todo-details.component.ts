import { Component, OnInit } from '@angular/core';
import {ToDo} from '../../model/todo.model';
import {ToDoApiService} from '../../model/todo-api.service';
import {ActivatedRoute, Router} from '@angular/router';
import { ToDoStoreService } from '../../model/todo-store.service';

@Component({
  selector: 'td-done-todos',
  templateUrl: './todo-details.component.html',
  providers: [ToDoApiService]
})
export class TodoDetailsComponent implements OnInit {

  currentToDo: ToDo;

  constructor(private todoStoreService: ToDoStoreService,
              private todoApiService: ToDoApiService,
              private route: ActivatedRoute,
              private router: Router) {}

  ngOnInit() {

    console.log('Snapshot:' + this.route.snapshot.paramMap.get('id'));
    this.route.paramMap.subscribe(p => {
      const toDoId = parseInt(p.get('id'), 10);
      this.loadToDo(toDoId);
    });

  }

  private loadToDo(id: number) {
    this.todoStoreService.getTodo(id)
      .subscribe(
        todo => this.currentToDo = todo
      );
  }

  updateToDo() {
    this.todoApiService.updateTodo(this.currentToDo)
      .subscribe();
  }

  backHome() {
    this.router.navigate(['/']);
    // this.router.navigateByUrl('/details/5');
  }
}
