import {CommonModule} from '@angular/common';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {SharedModule} from '../shared/shared.module';
import {TodoScreenComponent} from './todo-screen.component';
import {NewTodoComponent} from './new-todo/new-todo.component';
import { routing } from './todo-screen-routing.module';

@NgModule({
  declarations: [
    TodoScreenComponent,
    NewTodoComponent
  ],
  exports: [
    TodoScreenComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    RouterModule,
    SharedModule,
    routing
  ],
  providers: [],
})
export class TodoScreenModule { }
