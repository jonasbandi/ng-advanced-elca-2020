import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';


const routes: Routes = [
  { path: '', loadChildren: './todos/components/todo-screen/todo-screen.module#TodoScreenModule' },
  { path: 'done', loadChildren: './todos/components/done-screen/done-screen.module#DoneScreenModule' },
  { path: 'details', loadChildren: './todos/components/todo-details/todo-details.module#TodoDetailsModule' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
