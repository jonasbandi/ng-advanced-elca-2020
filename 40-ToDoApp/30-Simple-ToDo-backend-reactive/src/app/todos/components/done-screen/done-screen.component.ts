import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { ToDoStateService } from '../../model/todo-state.service';
import { BehaviorSubject, merge, Observable, of } from 'rxjs';
import { map, mergeMap, startWith, tap } from 'rxjs/operators';

interface DoneScreenVm {
  loading: boolean;
  todos: ToDo[];
}

@Component({
  template: `
      <section class="todoapp" *ngIf="doneScreenVm$ | async as vm">
          <td-todo-list *ngIf="!vm.loading" [todos]="vm.todos" (removeToDo)="removeToDo($event)"></td-todo-list>
          <div *ngIf="vm.loading" class="loading-placeholder">
              <td-spinner></td-spinner>
          </div>
      </section>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class DoneScreenComponent implements OnInit {

  doneScreenVm$: Observable<DoneScreenVm>;

  constructor(private todoService: ToDoStateService) {
    this.doneScreenVm$ = merge(
      of({loading: true, todos: []}),
      this.todoService.completedToDos$.pipe(map(todos => ({loading: false, todos})))
    )
      .pipe(
        tap(vm => console.log('VM', vm))
      );
  }

  ngOnInit() {
    this.todoService.loadToDos(true);
  }

  removeToDo(todo: ToDo) {
    this.todoService.deleteToDo(todo);
  }
}
