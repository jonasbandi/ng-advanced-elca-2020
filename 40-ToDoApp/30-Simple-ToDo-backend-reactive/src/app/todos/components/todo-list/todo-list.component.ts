import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-todo-list',
  template: `
      <ul class="todo-list">
          <td-todo-item *ngFor="let t of todos" [todo]="t" (removeToDo)="onRemoveToDo($event)"></td-todo-item>
      </ul>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoListComponent {


  @Input() todos: ToDo[];
  @Output() removeToDo = new EventEmitter<ToDo>();

  onRemoveToDo(todo) {
    this.removeToDo.emit(todo);
  }
}
