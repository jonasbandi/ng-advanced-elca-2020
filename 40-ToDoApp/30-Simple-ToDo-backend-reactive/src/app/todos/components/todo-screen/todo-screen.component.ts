import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ToDo } from '../../model/todo.model';
import { ToDoStateService } from '../../model/todo-state.service';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';

@Component({
  template: `
      <section class="todoapp" *ngIf="todos$ | async as todos">
          <td-new-todo [saving]="saving" (addToDo)="addToDo($event)"></td-new-todo>
          <td-todo-list *ngIf="!loading" [todos]="todos" (removeToDo)="completeToDo($event)"></td-todo-list>
      </section>
      <div *ngIf="loading" class="loading-placeholder">
          <td-spinner></td-spinner>
      </div>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoScreenComponent implements OnInit {

  loading = true;
  saving = false;
  todos$: Observable<ToDo[]>;

  constructor(private todoService: ToDoStateService) {
    this.todos$ = this.todoService.pendingToDos$
      .pipe(
        tap(() => {
          this.loading = false;
        })
      );
  }

  ngOnInit() {
    this.todoService.loadToDos(false);
  }

  addToDo(todo: ToDo) {
    this.todoService.addToDo(todo);
  }

  completeToDo(todo: ToDo) {
    this.todoService.completeToDo(todo);
  }

}
