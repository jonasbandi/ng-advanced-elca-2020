import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ToDo } from '../../model/todo.model';

@Component({
  selector: 'td-todo-item',
  template: `
      <li>
          {{todo.title}}
          <button *ngIf="todo.id" class="remove-button" (click)="onRemoveToDo(todo)">
              X
          </button>
          <div *ngIf="!todo.id" class="remove-button">
              <td-spinner [spinnerSize]="'30px'" [delay]="0"></td-spinner>
          </div>
      </li>
  `,
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class TodoItemComponent {

  @Input() todo: ToDo;
  @Output() removeToDo = new EventEmitter<ToDo>();

  onRemoveToDo() {
    this.removeToDo.emit(this.todo);
  }
}

// DEMO:
// delay the response on the server ... the x button is not displayed
