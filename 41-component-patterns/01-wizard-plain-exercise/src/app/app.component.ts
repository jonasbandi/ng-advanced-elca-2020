import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `

    <h1 *ngIf="screenIndex===0">First Screen</h1>
    <h1 *ngIf="screenIndex===1">Second Screen</h1>
    <h1 *ngIf="screenIndex===2">Third Screen</h1>
    <button (click)="goToNextScreen()">Next</button>
    <button (click)="goToPreviousScreen()">Previous</button>

  `,
  styles: []
})
export class AppComponent {
  screenIndex = 0;

  goToNextScreen() {
    this.screenIndex = (this.screenIndex + 1) % 3;
  }

  goToPreviousScreen() {
    this.screenIndex = (this.screenIndex + 3 - 1) % 3;
  }

}
