import { Component } from '@angular/core';

@Component({
  selector: 'aw-root',
  template: `

    <ng-template #step1 let-nav="nav">
      <div style="background-color: cyan">
        <h1>First Screen</h1>
        <a routerLink (click)="nav.goToNextScreen()">Next</a>
      </div>
    </ng-template>

    <ng-template #step2 let-nav="nav">
      <div style="background-color: yellow">
        <button (click)="nav.goToPreviousScreen()">Previous</button>
        <h1>Second Screen</h1>
        <a routerLink (click)="nav.goToNextScreen()">Next</a>
      </div>
    </ng-template>

    <ng-template #step3 let-nav="nav">
      <div style="background-color: mistyrose">
        <a routerLink (click)="nav.goToPreviousScreen()">Previous</a>
        <button (click)="nav.goToNextScreen()">Next</button>
        <h1>Third Screen</h1>
      </div>
    </ng-template>
    
    
    <h1>A wizard:</h1>

    <aw-wizard>
      <aw-wizard-step [screen]="step1">
      </aw-wizard-step>
      <aw-wizard-step [screen]="step2">
      </aw-wizard-step>
      <aw-wizard-step  [screen]="step3">
      </aw-wizard-step>
    </aw-wizard>
  `,
  styles: []
})
export class AppComponent {
  title = 'compound-wizard';
}
