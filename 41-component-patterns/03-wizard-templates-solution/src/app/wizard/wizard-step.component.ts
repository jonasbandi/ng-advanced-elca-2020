import { Component, Input, OnInit, Optional, TemplateRef } from '@angular/core';
import { WizardComponent } from './wizard.component';

@Component({
  selector: 'aw-wizard-step',
  template: `
    <div *ngIf="show">
      <ng-container *ngTemplateOutlet="screen; context:templateContext"></ng-container>
    </div>
  `,
  styles: []
})
export class WizardStepComponent implements OnInit {

  @Input() screen: TemplateRef<any>;

  templateContext = {
    nav: {
      goToNextScreen: () => this.wizard.goToNextScreen(),
      goToPreviousScreen: () => this.wizard.goToPreviousScreen(),
    }
  };

  constructor(private wizard: WizardComponent) {}

  get show() {
    return this.wizard.currentStep === this;
  }

  ngOnInit(): void {
    this.wizard.registerStep(this);
  }
}
