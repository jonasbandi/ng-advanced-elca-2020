import { AfterViewInit, Component, ContentChildren, forwardRef, OnInit, QueryList } from '@angular/core';
import { WizardStepComponent } from './wizard-step.component';

@Component({
  selector: 'aw-wizard',
  styles: [':host {display: block; border: 1px solid silver; padding: 20px}'],
  template: `
    <div>
      <ng-content></ng-content>
    </div>
  `,
})
export class WizardComponent {

  steps: WizardStepComponent[] = [];
  currentStep: WizardStepComponent;

  registerStep(step: WizardStepComponent) {
    if (this.steps.length === 0) {
      this.currentStep = step;
    }
    this.steps.push(step);
  }

  goToNextScreen() {
    if (this.steps.length > 1) {
      const currentIndex = this.steps.indexOf(this.currentStep);
      const nextIndex = (currentIndex + 1) % this.steps.length;

      this.currentStep = this.steps[nextIndex];
    }
  }

  goToPreviousScreen() {
    if (this.steps.length > 1) {
      const currentIndex = this.steps.indexOf(this.currentStep);
      let nextIndex = currentIndex - 1;
      if (nextIndex < 0) nextIndex += this.steps.length;

      this.currentStep = this.steps[nextIndex];
    }
  }

}
