import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { WizardComponent } from './wizard/wizard.component';
import { WizardStepComponent } from './wizard/wizard-step.component';
import { WizardNextComponent } from './wizard/wizard-next.component';
import { WizardPreviousComponent } from './wizard/wizard-previous.component';

@NgModule({
  declarations: [
    AppComponent,
    WizardComponent,
    WizardStepComponent,
    WizardNextComponent,
    WizardPreviousComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
