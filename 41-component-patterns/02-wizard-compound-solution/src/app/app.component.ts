import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <h1>A wizard:</h1>

<aw-wizard>
  <aw-wizard-step>
    <h1>First Screen</h1>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
  </aw-wizard-step>
  <aw-wizard-step>
    <h1>Second Screen</h1>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
  </aw-wizard-step>      
  <aw-wizard-step>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
    <h1>Third Screen</h1>
  </aw-wizard-step>
</aw-wizard>
  `,
  styles: []
})
export class AppComponent {
  title = 'compound-wizard';
}
