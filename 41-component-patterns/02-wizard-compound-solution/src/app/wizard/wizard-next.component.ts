import { Component, OnInit } from '@angular/core';
import { WizardComponent } from './wizard.component';

@Component({
  selector: 'aw-wizard-next',
  template: `
    <button (click)="wizard.goToNextScreen()">Next</button>
  `,
  styles: []
})
export class WizardNextComponent {

  constructor(public wizard: WizardComponent) {  }

}
