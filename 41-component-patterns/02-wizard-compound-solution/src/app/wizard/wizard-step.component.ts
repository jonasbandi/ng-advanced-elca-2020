import { Component, OnInit, Optional } from '@angular/core';
import { WizardComponent } from './wizard.component';

@Component({
  selector: 'aw-wizard-step',
  template: `
    <div>
      <ng-content *ngIf="show"></ng-content>
    </div>
  `,
  styles: []
})
export class WizardStepComponent implements OnInit {

  constructor(private wizard: WizardComponent) {}

  get show() {
    return this.wizard.currentStep === this;
  }

  ngOnInit(): void {
    this.wizard.registerStep(this);
  }
}
