import { Component, OnInit } from '@angular/core';
import { WizardComponent } from './wizard.component';

@Component({
  selector: 'aw-wizard-previous',
  template: `
    <button (click)="wizard.goToPreviousScreen()">Previous</button>
  `,
  styles: []
})
export class WizardPreviousComponent {

  constructor(public wizard: WizardComponent) {}

}
