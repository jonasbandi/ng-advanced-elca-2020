# Exercises Advanced Component Patterns


​	
## Exercise 1: Component Patterns - Compound Components

Start with the example in `41-Exercise\01-wizard-plain-exercise`.

Introduce components to model the "wizard", so that the final template looks like:

```
<aw-wizard>
  <aw-wizard-step>
    <h1>First Screen</h1>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
  </aw-wizard-step>
  <aw-wizard-step>
    <h1>Second Screen</h1>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
  </aw-wizard-step>      
  <aw-wizard-step>
    <aw-wizard-next></aw-wizard-next>
    <aw-wizard-previous></aw-wizard-previous>
    <h1>Third Screen</h1>
  </aw-wizard-step>
</aw-wizard>
```

## Exercise 2: Component Patterns - Working with `ng-template`

Implement the wizard from exercise 1.1 in a way that you can pass a `ng-template` to a `aw-wizard-step`:

```
<aw-wizard>
  <aw-wizard-step [screen]="step1">
  </aw-wizard-step>
  <aw-wizard-step [screen]="step2">
  </aw-wizard-step>
</aw-wizard>
```

The template should then be flexibel to implement the ui-controls that allow navigation between the screens.


## Exercise 3: Component Patterns - Dynamically create Components

Implement the wizard from exercise 1 by dynamically instantiating the wizard screens.

