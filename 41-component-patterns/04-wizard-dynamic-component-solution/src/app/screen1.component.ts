import { Component, OnInit } from '@angular/core';
import { IWizardNavigation, IWizardScreen } from './app.component';

@Component({
  styles: ['div {background-color: yellow}'],
  template: `
    <div>
      <h3>Screen 1</h3>
      <button (click)="navigation.goToNextScreen()">Next</button>
    </div>
  `
})

export class Screen1Component implements IWizardScreen {
  navigation: IWizardNavigation;
}
