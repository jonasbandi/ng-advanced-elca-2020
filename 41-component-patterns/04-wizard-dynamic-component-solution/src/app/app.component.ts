import { AfterViewInit, Component, ComponentFactoryResolver, OnInit, ViewChild, ViewContainerRef } from '@angular/core';
import { Screen1Component } from './screen1.component';
import { Screen2Component } from './screen2.component';

@Component({
  selector: 'aw-root',
  template: `

    <h1>A wizard:</h1>

    <ng-template #container></ng-template>
    
    <hr>

    <div>
      On the parent:
    </div>
    <button (click)="goToPreviousScreen()">Previous</button>
    <button (click)="goToNextScreen()">Next</button>

  `,
  styles: []
})
export class AppComponent implements OnInit, AfterViewInit {

  @ViewChild('container', {read: ViewContainerRef}) placeholder: ViewContainerRef;

  components = [
    Screen1Component,
    Screen2Component
  ];

  screenIndex = 0;

  navigation: IWizardNavigation = {
    goToNextScreen: () => this.goToNextScreen(),
    goToPreviousScreen: () => this.goToPreviousScreen()
  };

  constructor(private componentFactoryResolver: ComponentFactoryResolver) {
  }

  ngOnInit(): void {
    this.loadScreen();
  }

  ngAfterViewInit(): void {
    console.log(this.placeholder);
  }

  goToNextScreen() {
    this.screenIndex = (this.screenIndex + 1) % this.components.length;
    this.loadScreen();
  }

  goToPreviousScreen() {
    this.screenIndex = (this.screenIndex - 1 + this.components.length) % this.components.length;
    this.loadScreen();
  }

  private loadScreen() {

    const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.components[this.screenIndex]);

    this.placeholder.clear();
    const componentRef = this.placeholder.createComponent(componentFactory);
    (componentRef.instance as IWizardScreen).navigation = this.navigation;
  }

}


export interface IWizardScreen {
  navigation: IWizardNavigation;
}

export interface IWizardNavigation {
  goToNextScreen: () => void;
  goToPreviousScreen: () => void;
}
