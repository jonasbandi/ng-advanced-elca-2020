import { Component, OnInit } from '@angular/core';
import { IWizardNavigation, IWizardScreen } from './app.component';

@Component({
  styles: ['div {background-color: cyan}'],
  template: `
    <div>
      <h3>Screen 2</h3>
      <a routerLink (click)="navigation.goToNextScreen()">Next</a>
    </div>
  `
})

export class Screen2Component implements IWizardScreen {
  navigation: IWizardNavigation;
}
