import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { Screen1Component } from './screen1.component';
import { Screen2Component } from './screen2.component';

@NgModule({
  declarations: [
    AppComponent,
    Screen1Component, Screen2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  entryComponents: [Screen1Component, Screen2Component],
  bootstrap: [AppComponent]
})
export class AppModule { }
