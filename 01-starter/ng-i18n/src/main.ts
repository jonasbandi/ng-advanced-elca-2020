import { enableProdMode } from '@angular/core';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import { environment } from './environments/environment';
import { loadTranslations } from '@angular/localize';

if (environment.production) {
  enableProdMode();
}

// loading translations at runtime
// this could be dynamic, however it cant be changed after Angular has started
loadTranslations({
  '2580107352240924543': 'öppis ganz angers',
  '74a288d2244552035ee2dab4dd11d7a73f2f2d90': 'Das geit scho!'
});

platformBrowserDynamic()
  .bootstrapModule(AppModule)
  .catch(err => console.error(err));
