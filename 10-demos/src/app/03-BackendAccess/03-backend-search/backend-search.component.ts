import { Component, OnInit } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, switchMap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';

type SearchResponse = [string, string[], string[], string[]];

@Component({
  selector: 'aw-backend-search',
  template: `
    <h2>Wikipedia Search</h2>
    <input #searchTerm (input)="searchTermSubject.next(searchTerm.value)" />
    <ul>
      <li *ngFor="let item of items">
        {{ item }}
      </li>
    </ul>
  `
})
export class BackendSearchComponent implements OnInit {
  searchTermSubject = new Subject<string>();
  items: string[] = [];

  constructor(private http: HttpClient) {}

  ngOnInit(): void {
    this.searchTermSubject
      .pipe(
        debounceTime(400),
        distinctUntilChanged(),
        switchMap(term => this.searchWikipedia(term))
      )
      .subscribe((results: string[]) => (this.items = results));
  }

  searchWikipedia(term: string): Observable<string[]> {
    const queryUrl = `https://en.wikipedia.org//w/api.php?action=opensearch&format=json&origin=*&maxlag=17&search=${term}`;

    return this.http.get<SearchResponse>(queryUrl).pipe(map(response => response[1]));
  }
}
