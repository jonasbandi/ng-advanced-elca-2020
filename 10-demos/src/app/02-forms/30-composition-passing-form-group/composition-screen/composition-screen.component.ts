import { Component } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';

@Component({
  template: `
    <h1>Decomposed Form</h1>
    <form [formGroup]="myForm" (ngSubmit)="onSubmit()">
      <label>Person</label>
      <div formGroupName="person">
        <input formControlName="firstName" />
        <input formControlName="lastName" />
      </div>

      <aw-form-section [addressFormGroup]="$any(myForm.controls['address'])"></aw-form-section>

      <br />
      <button type="submit">Submit</button>
    </form>
  `
})
export class CompositionScreenComponent {
  myForm: FormGroup;

  constructor() {
    this.myForm = new FormGroup({
      person: new FormGroup({
        firstName: new FormControl(''),
        lastName: new FormControl('')
      }),
      address: new FormGroup({})
    });
  }

  onSubmit(): void {
    console.log(this.myForm.value);
  }
}
