import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, ValidatorFn, Validators } from '@angular/forms';

@Component({
  templateUrl: 'dynamic-form.component.html',
  styleUrls: ['dynamic-form.component.scss']
})
export class DynamicFormComponent implements OnInit {
  formDescription = staticFormDescription; // could be loaded from DB //tslint
  form: FormGroup = new FormGroup({});
  formTemplateFields: TemplateControlDescription[] = [];

  ngOnInit(): void {
    for (const [fieldName, fieldDescription] of Object.entries(this.formDescription)) {
      const formControl = new FormControl(fieldDescription.value, mapValidator(fieldDescription.validators));
      this.form.addControl(fieldName, formControl);

      this.formTemplateFields.push({
        key: fieldName,
        label: fieldDescription.label,
        type: fieldDescription.type,
        options: fieldDescription.options
      });
    }
  }
}

const staticFormDescription: { [key: string]: IFieldDescription } = {
  firstname: {
    label: 'Firstname',
    value: 'Tyler',
    type: 'text',
    validators: {
      required: true
    }
  },
  age: {
    label: 'Age',
    value: 40,
    type: 'number',
    validators: {
      min: 18
    }
  },
  gender: {
    label: 'Gender',
    value: 'F',
    type: 'radio',
    options: [
      { label: 'Male', value: 'M' },
      { label: 'Female', value: 'F' }
    ]
  },
  city: {
    label: 'City',
    value: '',
    type: 'select',
    options: [
      { label: '(choose one)', value: '' },
      { label: 'New York', value: 'NY' },
      { label: 'Los Angeles', value: 'LA' },
      { label: 'Salt Lake City', value: 'SLC' }
    ]
  }
};

function mapValidator(validatorDescription?: ValidatorDescription): ValidatorFn[] {
  const validatorFns: ValidatorFn[] = [];
  if (validatorDescription) {
    Object.keys(validatorDescription).forEach(validationType => {
      if (validationType === 'required') {
        validatorFns.push(Validators.required);
      } else if (validationType === 'min') {
        validatorFns.push(Validators.min(validatorDescription[validationType] as number));
      }
    });
  }
  return validatorFns;
}

type FormFieldType = 'text' | 'number' | 'radio' | 'select';

type FormFieldOptions = { label: string; value: string }[];

type ValidatorDescription = { required?: boolean; min?: number };

interface IFieldDescription {
  label: string;
  value: string | number;
  type: FormFieldType;
  options?: FormFieldOptions;
  validators?: ValidatorDescription;
}

interface TemplateControlDescription {
  key: string;
  label: string;
  type: FormFieldType;
  options?: FormFieldOptions;
}
