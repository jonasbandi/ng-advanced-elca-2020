import { Component } from '@angular/core';

@Component({
  selector: 'aw-mouse-tracker',
  template: `
    <aw-mouse #mouse="mouse">
      <p>The mouse position is {{ mouse.state.x }}, {{ mouse.state.y }}</p>
    </aw-mouse>
  `
})
export class MouseTrackerComponent {}
